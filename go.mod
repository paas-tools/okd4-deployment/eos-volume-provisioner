module eos-volume-provisioner

go 1.16

require (
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529
	github.com/spf13/pflag v1.0.5 // indirect
	k8s.io/api v0.21.0
	k8s.io/apimachinery v0.21.0
	k8s.io/client-go v0.21.0
	sigs.k8s.io/sig-storage-lib-external-provisioner/v6 v6.3.0
)
