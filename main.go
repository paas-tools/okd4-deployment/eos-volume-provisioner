/*
Copyright 2021 CERN.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"flag"
	"os"

	"github.com/golang/glog"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/sig-storage-lib-external-provisioner/v6/controller"
)

var (
	EOS_HOSTPATH               = os.Getenv("EOS_HOSTPATH")
	EOS_VOLUMEPROVISIONER_NAME = os.Getenv("EOS_VOLUMEPROVISIONER_NAME")
	master                     = flag.String("master", "", "Master URL to build a client config from. Either this or kubeconfig needs to be set if the provisioner is being run out of cluster.")
	kubeconfig                 = flag.String("kubeconfig", "", "Absolute path to the kubeconfig file. Either this or master needs to be set if the provisioner is being run out of cluster.")
)

// EOSVolumeProvisioner represents a VolumeProvisioner for EOS.
type eosVolumeProvisioner struct {
	client kubernetes.Interface // Kubernetes client for accessing the cluster during provision
}

func NewEOSVolumeProvisioner(client kubernetes.Interface) controller.Provisioner {
	return &eosVolumeProvisioner{
		client: client,
	}
}

// Provision takes care of creating an EOS v1.PersistentVolume using a hostPath to `/eos/`.
// For EOS requests there is nothing to provisioning.
func (vp *eosVolumeProvisioner) Provision(ctx context.Context, provisionOptions controller.ProvisionOptions) (*v1.PersistentVolume, controller.ProvisioningState, error) {

	// Create EOS PV and bind it to an existing PVC
	hostPathType := v1.HostPathDirectory

	pv := &v1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name: provisionOptions.PVName,
		},
		Spec: v1.PersistentVolumeSpec{
			Capacity: v1.ResourceList{
				v1.ResourceName(v1.ResourceStorage): provisionOptions.PVC.Spec.Resources.Requests[v1.ResourceName(v1.ResourceStorage)],
			},
			// EOS is read/write, so it supports all 3 access modes
			AccessModes: []v1.PersistentVolumeAccessMode{v1.ReadOnlyMany, v1.ReadWriteMany, v1.ReadWriteOnce},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				HostPath: &v1.HostPathVolumeSource{
					Path: EOS_HOSTPATH,
					Type: &hostPathType,
				},
			},
			ClaimRef: &v1.ObjectReference{
				Kind:      "PersistentVolumeClaim",
				Name:      provisionOptions.PVC.GetName(),
				Namespace: provisionOptions.PVC.GetNamespace(),
			},
			// Set ReclaimPolicy to Delete so that we get rid off PV resources whenever a PVC is deleted.
			// EOS PVs hold no data since they are just a hostpath to the /var/eos shared mount point,
			// so there is no data to delete - the Delete ReclaimPolicy just cleans up the unneeded PV resources.
			PersistentVolumeReclaimPolicy: "Delete",
		},
	}
	// Return the volume. Please note it has not been saved yet so at this point it
	// still does not exist
	return pv, controller.ProvisioningFinished, nil
}

// Delete removes the physical volume associated with the PV. For EOS volumes
// there is nothing to do so the funcion just returns nil
func (vp *eosVolumeProvisioner) Delete(ctx context.Context, pv *v1.PersistentVolume) error {
	return nil
}

func main() {

	flag.Set("logtostderr", "true")
	flag.Parse()

	// creates the connection
	var config *rest.Config
	var err error

	if *master != "" || *kubeconfig != "" {
		glog.Infof("Using out-of-cluster configuration")
		config, err = clientcmd.BuildConfigFromFlags(*master, *kubeconfig)
	} else {
		glog.Infof("Using in-cluster configuration; use -master or -kubeconfig to change")
		config, err = rest.InClusterConfig()
	}

	if err != nil {
		glog.Fatalf("Failed to create config: %v", err)
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatalf("Failed to create client: %v", err)
	}

	// Get server verion from client
	// because the provision controller needs it
	kubeVersion, err := clientset.Discovery().ServerVersion()
	if err != nil {
		glog.Errorln("Could not fetch server version.")
	}

	eosVolumeProvisioner := NewEOSVolumeProvisioner(clientset)

	// Start the provision controller which will dynamically provision EOS volumes
	provisionController := controller.NewProvisionController(
		clientset,
		EOS_VOLUMEPROVISIONER_NAME, //provisioner name. See storageclass.yaml
		eosVolumeProvisioner,
		kubeVersion.GitVersion,
		controller.LeaderElection(false))

	glog.Infof("Controller created. Details:\n%+v\nRun started...", provisionController)
	glog.Flush()

	provisionController.Run(context.Background())
}
