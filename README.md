# EOS volume provisioner

Kubernetes controller to dynamically provision EOS PVs based on PVC events (for EOS Automounter).

It is based on the upstream [external-provisioner](https://github.com/kubernetes-csi/external-provisioner) controller.

This controller is deployed from `okd4-install` project as part of the `eosxd` application, located under [https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/chart/charts/eosxd](https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/chart/charts/eosxd).
